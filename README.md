# Secord HFD Obesity TGFA

* Knit the code `AngiogenicGene-BMI-OS-assoc-2022-containerver.Rmd` to generate the report `AngiogenicGene-BMI-OS-assoc-2022-containerver.html` along with all figures and supplementary tables.
    + If the singularity container is not used for knitting, change the following scripts in the `Load packages` section: 
        - library(preprocessCore, lib.loc = "/Rtmplib") -> library(preprocessCore)
        - library(affy, lib.loc = "/Rtmplib") -> library(affy)
    + The reproducibility may not be achieved without the singularity container. 
* Alternatively, if you are working with ubuntu system, modify the path in the Makefile and run `make report2022container` to run the code `AngiogenicGene-BMI-OS-assoc-2022-containerver.Rmd`.
