sif=/data1/workspace/DCI/Reihani/BMI_ContinuedAnalysis/Code/dcibioinformaticsR-v2.0.sif
data=/data1/workspace/DCI/Reihani/BMI_ContinuedAnalysis/ # The folder with the raw data
wd=/data1/workspace/DCI/Reihani/BMI_ContinuedAnalysis/Code # The folder where the code was downloaded to
tmp=/data1/workspace/DCI/Reihani/BMI_ContinuedAnalysis/Code/ExtraRLib:/Rtmplib # The folder where the extra packages are downloaded to 
tm=$$(date '+%Y-%m-%d_%H-%M-%S')



report2022container:
	singularity exec \
		--bind ${data} \
		--bind ${tmp} \
		${sif} \
		R -e "library(knitr); rmarkdown::render('${wd}/AngiogenicGene-BMI-OS-assoc-2022-containerver.Rmd',~+~~+~encoding~+~=~+~'UTF-8')" >log_${tm}.txt

